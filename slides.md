# Who Am I?
* Chris Fedde
* chris@fedde.us
* System programmer
* System administrator

# Who Am I?
* Chris Fedde
* DevOps
* Developer
* In an Operations team

# Ground Rules 
* Open forum
* Ask, Interrupt, Comment, Kibitz

# Ground Rules
* Tell me when I'm making a mistake
* You'll get extra credit!

# Ground Rules
* Breaks every hour ( set timer )
* Afters 
* El Noa Noa

# About You
* Who are you
* What brings you here

# Assumptions
* You have a reasonable grasp of the linux/unix command line
* what is a pipe?
* how to navigate directories
* how to run command lines

# Assumptions
* You have an ssh client
* with you today
* that can access the local network

# Assumptions
* Pair Admin
* Like Pair Programming
* Solves the cross training problem


# Assumptions
* [DDG](https://duckduckgo.com/)
* [Ansible Documentation](http://docs.ansible.com/)

# Availability
* This courseware is freely available
* [https://gitlab.com/cfedde/aghi_ansible](https://gitlab.com/cfedde/aghi_ansible)

# License
* Many components I`ve used have their own copyright and license 
* All software used is free (lebre/gratis)
* This courseware is placed in the public domain.

# Any questions before we get into it?

# Manage a complex of servers
* Maybe a large number of servers
* With minimal personnel

# Words
* Grouping words for a bunch of servers
* What to call a bunch of servers?

# Words
What to call a bunch of servers?

* Cluster
* Farm
* Pile
* Complex

# Words
* "Server Complex"
* I like this term better
* "Administrative Domain"

# "Server Complex"
## "Administrative Domain"
* kinda like a broadcast domain
* from networking?
* does that help?
* No?

# "Server Complex"
## "Administrative Domain"
* Some number of manageable elements
* Supported by a common team of admins
* Shared access policies
* The machines where you change root password when someone leaves

# Managing the server complex
* How did we get here
* Brand new fresh deployment
* Old stinky boxes with almost no conventions
* Applances
* Faberge Eggs

# Thought Experiment
* The Heaping Pile of Servers
* Engineers
* No one has done any admin
* Everyone has root
* Engineering in Place (EIP)
* Discovery

# Solving the Problem
* Discipline
* Tools
* Automation
* Conventions
* Contain Complexity
* Build on good solutions
* Build on good practice

# Solving the Problem
* Tools everyone understands
* Best practices
* Revision Control 

# Emulation

[em-yuh-ley-shuh n]

noun

1.  effort or desire to equal or excel others.
2.  [Obsolete]. jealous rivalry.
3.  [IT] virutal machines

# Emulation

Emulating a complex system using a simple analog.


# Emulation

Don't try this in production.


# Emulation
* For purposes of Education Only
* "Reducto ad absurdum"
* What is the smallest possible environment that could possibly work?

# Emulation
* Vagrant
* Docker

# Emulation
* Vagrant Guests on my laptop
* Docker containers for "managed hosts"

( Demo 1 )

# About Ansible
* The Name
* The Configuration Management Tool
( ddg )

# About Ansible
* Fictional "superluminal" radio
* Faster than light
* quantum entanglement
* bla bla bla

# About Ansible
* Orson Scott Card
* Enders Game
* Block buster movie 2013
* Book 1977

# About Ansible
* Ursula LeGuin
* Rocannan`s World 
* Book 1966
* Left Hand of Darkness 
* Short stories

# About Ansible
* A tool for system configuration management
* Pythonic

# About Ansible
## Alternatives
* A bunch of scripts using scp and ssh in a loop
* A slightly smaller set of scripts using pscp and pssh 

# About Ansible
## Alternatives
* cf-engine
* puppet
* rdist
* chef
* salt stack
* re(x)

# About Ansible
## Alternatives
* docker
* Virtualization
* VMWare/VirtualBox/OpenStack/AWS/"other"

# About Ansible
* Push vs pull
* "Masterless"
* ansible-pull

# About Ansible
## Requirements
* Manager 
* Where the ansible command line is run

# About Ansible
## Requirements
* Targets
* the managed hosts

# About Ansible
## Manager Requirements
* Modern Linux (about 2005 or so)
* Ansible package
* Package management
* Revision control (git)

# About Ansible
## Targets Requirements 
* The Managed Hosts
* Python 2.3 or better (a json module)
* sshd
* Some login user
* sudo/su if needed.

# About Ansible
* Two was of using it
* ansible (adhoc)

# About Ansible
* Two was of using it
* ansible-playbook (playbook)

# About Ansible
* ad hoc.
* run a simple module "task" on some list of hosts

# About Ansible
* ansible-playbook
* Run a set of related tasks over a lists of

# About Ansible
* Inventory 
* Modules
* Playbooks

# About Ansible
* Inventory 
* the list managed systems
* with grouping and so on

# Inventory
* Can be plugged into your cloud service
* Or "just a file" with some structure.
( Look at a simple file )

# About Ansible
* [Modules](http://docs.ansible.com/ansible/modules_by_category.html)
* The things ansible can do
( look at the list of modules )

# About Ansible
* Playbooks
* instructions for ansible-playbook
* YAML format

# Finish your environmnt

Ask me for your own personal IP address

  ssh vagrant@yourip
  cp -r /vagrant class
  cd class
  source environment

# Emulated Network 
* Vagrant guest
* five target 'servers'.

# Look at our environment

   env | grep ANSIBLE

# What are the names of your managed hosts?

  cat etc/ansible/hosts

Note the funny names for the hosts.  Those are actually the short names for 
docker containers. 

# Count the number of files in /etc on the class servers

   ansible class -m shell -a 'ls /etc | wc -l'

# Add db and web and app groups to the hosts file

Two nodes in db and two in web and an app group made by including the other two groups

    vi etc/ansible/host

When done it might look something like this.  Be suue 

    localhost

    [class]
    daa87bc9f63c
    e4d3a8708d36
    d17ad6801c24
    f463acd92e46
    0f3514a36b97

    [db]
    daa87bc9f63c
    e4d3a8708d36

    [web]
    d17ad6801c24
    f463acd92e46

    [app:children]
    db
    web

# Host Patterns

Ansible supports a pretty complex host pattern language

   ansible app -m ping

   ansible class:\!db -m ping

   ansible *e* -m shell uptime

# What Facts do we have?

    ansible class:\!app -m setup | less

# Run as user root

    ansible class -m script -a bin/i_am_root

# A Playbook
* install and configure NTP

